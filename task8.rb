class Sample

 def count_frequency
  arr = ["Jason", "Jason", "Teresa", "Judah", "Michelle", "Judah", "Judah", "Allison"]
  freq = arr.inject(Hash.new(0)) { |h,v| h[v] += 1; h}
  puts freq
 end
end

object = Sample. new
object.count_frequency
