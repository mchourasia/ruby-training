
class Sample
   def print_numbers
      (0..100).each do |i|
         puts "Value of local variable is #{i}"
      end
   end
end

# Now using above class to create objects
object = Sample.new
object.print_numbers
