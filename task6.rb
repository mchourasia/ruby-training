class Sample 
 require 'csv'
 
 #Create a csv
def create_csv 
 CSV.open('test.csv', 'wb') do |csv|
 end
end

#Write in csv
def write_csv 
   CSV.open('test.csv', 'wb') do |csv|
      csv  << [1,2]
      csv  << [2,2]       
      csv  << [3,2]       
      csv  << [4,2]       
      csv  << [5,2]              
 end
end

# Read csv file and append to it
def read_csv 

 CSV.foreach('test.csv') do |row|
  sum = row[0].to_i * row[1].to_i
  CSV.open("test.csv", "a+") do |csv|
   csv << [sum]

  end
 end
end

end

object = Sample. new
object.create_csv
object.write_csv
object.read_csv

